<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE>
<html>
<head>
    <spring:url value="/static/js/jquery-3.4.1.min.js" var="jqueryJs" />
    <spring:url value="/static/css/bootstrap.min.css" var="bootstrapCss" />
    <spring:url value="/static/js/bootstrap.min.js" var="bootstrapJs" />
    <spring:url value="/static/js/feather.min.js" var="featherJs" />
    <link rel="stylesheet" type="text/css" href="${bootstrapCss}"/>
    <script src="${jqueryJs}"></script>
    <script src="${bootstrapJs}"></script>
    <script src="${featherJs}"></script>
</head>
<body>
	<jsp:include page="../layout/layout.jsp"></jsp:include>
    	
    <div class="container-fluid">
		<div class="row">
			<nav class="col-md-2 d-none d-md-block bg-light sidebar">
				<div class="sidebar-sticky">
					<ul class="nav flex-column">
						<li class="nav-item">
							<a class="nav-link active" href="http://localhost:8085/MiniCell">
								<span data-feather="home"></span> Master Lookup
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="http://localhost:8085/MiniCell/customer">
								<span data-feather="users"></span> Customers 
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="http://localhost:8085/MiniCell/topupaccount">
								<strong><span data-feather="arrow-up-circle"></span> Top-up Account</strong>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="http://localhost:8085/MiniCell/transfer">
								<span data-feather="refresh-cw"></span> Transfer within Account
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">
								<span data-feather="dollar-sign"></span> Bill Transaction
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">
								<span data-feather="dollar-sign"></span> Bill Payment
							</a>
						</li>
					</ul>
				</div>
			</nav>
    	
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            	<h4>Top-up Account</h4    >
            	<div class="btn-toolbar mb-2 mb-md-0">
           			<div class="btn-group mr-2">
                  		<a href="<c:url value='/topupaccount/topup'/>" class="btn btn-sm btn-outline-secondary">Top-up</a>
              		</div>
            	</div>
          	</div>
          	
			<c:url var="search_url"  value="/topupaccount/search" />
	        <form:form action="${search_url}" method="get" modelAttribute="topup">
	        	<div class="form-group">
        			<label for="accountNumber">Account Number</label>
        			<form:input type="text" class="form-control" path="accountNumber"/>
	        	</div>
	        	<label for="amount">Amount</label>
	        	<div class="form-group row">
	        		<div class="col-sm-5">
	        			<form:input type="number" class="form-control" path="amount1"/>
	        		</div>
	        		<div class="col-sm-2">
	        			<p class="text-center"> - </p>
	        		</div>
	        		<div class="col-sm-5">
	        			<form:input type="number" class="form-control" path="amount2"/>
	        		</div>
	        	</div>
	            <button type="submit" class="btn btn-primary">Search</button>
	        </form:form>
	        <hr/>
          	
			<c:choose>
            	<c:when test="${not empty message && message.status eq 'SUCCESS'}">
            		<div class="alert alert-success" role="alert">
            			${message.description}
                	</div>
				</c:when>
				<c:when test="${not empty message && message.status eq 'ERROR'}">
					<div class="alert alert-error" role="alert">
						${message.description}
					</div>
				</c:when>
			</c:choose>
			
			<h5>List</h5>
				<div class="table-responsive">
					<table class="table table-striped table-sm">
						<thead>
						  <tr>
						    <th>Transaction Date</th>
						    <th>Account Number</th>
						    <th>Amount</th>
						    <th>Berita</th>
						  </tr>
						</thead>
						<tbody>
    						<c:choose>
							<c:when test="${not empty topupList}">
								<c:forEach var="topup" items="${topupList}">
									<tr>
										<td><fmt:formatDate pattern = "dd-MM-yyyy" value = "${topup.transactionDate}" /></td>
										<td>${topup.account.accountNumber}</td>
										<td>${topup.amount}</td>
										<td>${topup.berita}</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="4">No data</td>
								</tr>
							</c:otherwise>
							</c:choose>
          				</tbody>
        			</table>
      			</div>
    		</main>
  		</div>
	</div>
    
    <script>
        $(document).ready(function(){
           feather.replace();
        });
    </script>
</body>
</html>