<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE>
<html>
<head>
    <spring:url value="/static/js/jquery-3.4.1.min.js" var="jqueryJs" />
    <spring:url value="/static/css/bootstrap.min.css" var="bootstrapCss" />
    <spring:url value="/static/js/bootstrap.min.js" var="bootstrapJs" />
    <spring:url value="/static/js/feather.min.js" var="featherJs" />
    <link rel="stylesheet" type="text/css" href="${bootstrapCss}"/>
    <script src="${jqueryJs}"></script>
    <script src="${bootstrapJs}"></script>
    <script src="${featherJs}"></script>
</head>
<body>
	<jsp:include page="layout/layout.jsp"></jsp:include>
	
	<div class="container-fluid">
	<div class="row">
		<nav class="col-md-2 d-none d-md-block bg-light sidebar">
			<div class="sidebar-sticky">
				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link active" href="http://localhost:8085/MiniCell">
							<strong><span data-feather="home"></span> Master Lookup</strong>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="http://localhost:8085/MiniCell/customer">
							<span data-feather="users"></span> Customers
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="http://localhost:8085/MiniCell/topupaccount">
							<span data-feather="arrow-up-circle"></span> Top-up Account
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="http://localhost:8085/MiniCell/transfer">
							<span data-feather="refresh-cw"></span> Transfer within Account
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">
							<span data-feather="dollar-sign"></span> Bill Transaction
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">
							<span data-feather="dollar-sign"></span> Bill Payment
						</a>
					</li>
				</ul>
			</div>
		</nav>
    
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h4>Master Lookup</h4    >
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                  <a href="<c:url value='/lookup/create'/>" class="btn btn-sm btn-outline-secondary">Create</a>
              </div>
            </div>
          </div>
          <c:url var="search_url"  value="/lookup/search" />
          <form:form action="${search_url}" method="get" modelAttribute="lookupDto">
              <div class="form-group">
                <label for="code">Code</label>
                <form:input type="text" class="form-control" path="code"/>
              </div>
              <div class="form-group">
                <label for="description">Description</label>
                <form:input type="text" class="form-control" path="description"/>
              </div>
              <button type="submit" class="btn btn-primary">Search</button>
          </form:form>
          <hr/>
          
           <c:choose>
              <c:when test="${not empty message && message.status eq 'SUCCESS'}">
                  <div class="alert alert-success" role="alert">
                  ${message.description}
                </div>
              </c:when>
              <c:when test="${not empty message && message.status eq 'ERROR'}">
                  <div class="alert alert-error" role="alert">
                  ${message.description}
                </div>
              </c:when>
          </c:choose>
          <h5>List</h5>
          <div class="table-responsive">
            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th>Code</th>
                  <th>Description</th>
                  <th>&nbsp;</th>
                </tr>
              </thead>
              <tbody>
                  <c:choose>
                      <c:when test="${not empty lookupList}">
                          <c:forEach var="lookup" items="${lookupList}">
                            <tr>
                              <td>${lookup.code}</td>
                              <td>${lookup.description}</td>
                              <td>
                                  <a href="<c:url value='/lookup/${lookup.code}'/>"><span data-feather="eye">&nbsp;</span></a>
                                  <a href="<c:url value='/lookup/edit/${lookup.code}'/>"><span data-feather="edit">&nbsp;</span></a>
                                  <a href="" data-toggle="modal" data-target="#deleteConfirm" onClick="onDelete('${lookup.code}')"><span data-feather="trash">&nbsp;</span></a>
                              </td>
                            </tr>
                        </c:forEach>
                      </c:when>
                      <c:otherwise>
                          <tr>
                          <td colspan="2">No data</td>
                        </tr>
                      </c:otherwise>
                  </c:choose>
              </tbody>
            </table>
          </div>
        </main>
      </div>
    </div>
    
    <div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="deleteConfirmLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <c:url var="delete_url"  value="/lookup/delete" />
          <form action="${delete_url}" method="post">
              <div class="modal-header">
                <h5 class="modal-title" id="deleteConfirmLabel">Delete Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                Are you sure to delete this item?
                <input type="text" name="deleted_code" hidden="true"/>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary" id="btnDeleteConfirm">Yes</button>
              </div>
          </form>
        </div>
      </div>
    </div>
    <script>
        $(document).ready(function(){
           feather.replace();
        });
        
        function onDelete(code) {
            $("input[type=text][name=deleted_code]").val(code);
        }
    </script>
</body>
</html>