<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <spring:url value="/static/js/jquery-3.4.1.min.js" var="jqueryJs" />
    <spring:url value="/static/css/bootstrap.min.css" var="bootstrapCss" />
    <spring:url value="/static/css/bootstrap-datepicker.css" var="bootstrapDatepickerCss" />
    <spring:url value="/static/js/bootstrap.min.js" var="bootstrapJs" />
    <spring:url value="/static/js/feather.min.js" var="featherJs" />
    <spring:url value="/static/js/bootstrap-datepicker.js" var="bootstrapDatepickerJs" />
    <link rel="stylesheet" type="text/css" href="${bootstrapCss}"/>
    <link rel="stylesheet" type="text/css" href="${bootstrapDatepickerCss}"/>
    <script src="${jqueryJs}"></script>
    <script src="${bootstrapJs}"></script>
    <script src="${featherJs}"></script>
    <script src="${bootstrapDatepickerJs}"></script>
    <style>
      	.error {color:red}
  	</style>
</head>
<body>
	<jsp:include page="../layout/layout.jsp"></jsp:include>
	
	<div class="container-fluid">
		<div class="row">
			<nav class="col-md-2 d-none d-md-block bg-light sidebar">
				<div class="sidebar-sticky">
					<ul class="nav flex-column">
						<li class="nav-item">
							<a class="nav-link active" href="http://localhost:8085/MiniCell">
								<span data-feather="home"></span> Master Lookup
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="http://localhost:8085/MiniCell/customer">
								<strong><span data-feather="users"></span> Customers </strong>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="http://localhost:8085/MiniCell/topupaccount">
								<span data-feather="arrow-up-circle"></span> Top-up Account
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="http://localhost:8085/MiniCell/transfer">
								<span data-feather="refresh-cw"></span> Transfer within Account
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">
								<span data-feather="dollar-sign"></span> Bill Transaction
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">
								<span data-feather="dollar-sign"></span> Bill Payment
							</a>
						</li>
					</ul>
				</div>
			</nav>
    
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h4>Customer</h4>
          </div>
          <form:form id="form" action="" method="post" modelAttribute="customer">
              <div class="form-group">
                <label for="description">Customer Name</label>
                <form:input type="text" class="form-control" path="name"/>
                <form:errors path="name" cssClass="error"/>
              </div>
              <div class="form-group">
                <label for="description">Identity Type</label>
                <div class="input-group">
                    <form:hidden class="form-control" path="identityType"/>
                    <form:input class="form-control" path="identityTypeDescription" readonly="true"/>
                    <form:errors path="identityTypeDescription" cssClass="error"/>
                    <div class="input-group-append">
                        <button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#chooseIdentity">Choose</button>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <label for="description">Identity Number</label>
                <form:input type="text" class="form-control" path="identityNumber"/>
                <form:errors path="identityNumber" cssClass="error"/>
              </div>
              <div class="dates form-group">
                <label for="description">Birth Date</label>
                <form:input id="user"  type="text" class="form-control" placeholder="dd-mm-yyy" path="birthDate"/>
                <form:errors path="birthDate" cssClass="error"/>
              </div>
              <form:hidden class="form-control" path="id"/>
              <button type="submit" class="btn btn-primary" onClick="onSubmit()">Submit</button>
              <form:input path="selected" hidden="true"/>
              <form:input path="action" hidden="true"/>
          </form:form>
        </main>
      </div>
    </div>
    
    <div class="modal fade" id="chooseIdentity" tabindex="-1" role="dialog" aria-labelledby="chooseIdentityLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="chooseIdentityLabel">Identity Type</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                  <thead>
                    <tr>
                      <th>Code</th>
                      <th>Description</th>
                      <th>&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                      <c:choose>
                          <c:when test="${not empty identityTypeList}">
                              <c:forEach var="identityType" items="${identityTypeList}" varStatus="status">
                                <tr>
                                  <td>${identityType.code}</td>
                                  <td>${identityType.description}</td>
                                  <td>
                                      <button class="btn btn-sm btn-secondary" onClick="chooseIdentityType('${identityType.code}')" data-dismiss="modal">Choose</button>
                                  </td>
                                </tr>
                            </c:forEach>
                          </c:when>
                      </c:choose>
                  </tbody>
                </table>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    
    <script>
        function chooseIdentityType(code) {
            $("input[type=text][name=action]").val("choose_identity_type");
            $("input[type=text][name=selected]").val(code);
            $("#form").submit();
        }
        function onSubmit() {
            $("input[type=text][name=action]").val("submit");
        }
        $(document).ready(function(){
           feather.replace();
        });
        $(function(){
        	$('.dates #user').datepicker({
        		'format' : 'dd-mm-yyyy',
        		'autoclose' : true
        	})
        })
    </script>
</body>
</html>