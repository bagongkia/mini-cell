<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE HTML>
<html>
<head>
    <spring:url value="/static/js/jquery-3.4.1.min.js" var="jqueryJs" />
    <spring:url value="/static/css/bootstrap.min.css" var="bootstrapCss" />
    <spring:url value="/static/js/bootstrap.min.js" var="bootstrapJs" />
    <spring:url value="/static/js/feather.min.js" var="featherJs" />
    <link rel="stylesheet" type="text/css" href="${bootstrapCss}"/>
    <script src="${jqueryJs}"></script>
    <script src="${bootstrapJs}"></script>
    <script src="${featherJs}"></script>
</head>
<body>
	<jsp:include page="../layout/layout.jsp"></jsp:include>
	
	<div class="container-fluid">
	<div class="row">
		<nav class="col-md-2 d-none d-md-block bg-light sidebar">
			<div class="sidebar-sticky">
				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link active" href="http://localhost:8085/MiniCell">
							<span data-feather="home"></span> Master Lookup
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="http://localhost:8085/MiniCell/customer">
							<strong><span data-feather="users"></span> Customers</strong>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="http://localhost:8085/MiniCell/topupaccount">
							<span data-feather="arrow-up-circle"></span> Top-up Account
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="http://localhost:8085/MiniCell/transfer">
							<span data-feather="refresh-cw"></span> Transfer within Account
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">
							<span data-feather="dollar-sign"></span> Bill Transaction
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">
							<span data-feather="dollar-sign"></span> Bill Payment
						</a>
					</li>
				</ul>
			</div>
		</nav>
    
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h4>Customer</h4>
          </div>
          
          <form:form id="form" action="" method="post" modelAttribute="customer">
          	<table>
			  <tr>
			    <td>Customer Name</td>
			    <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
			    <td>${customer.name}</td>
			  </tr>
			  <tr>
			    <td>Identity Type</td>
			    <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
			    <td>${lookupDetail.description}</td>
			  </tr>
			  <tr>
			    <td>Identity Number</td>
			    <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
			    <td>${customer.identityNumber}</td>
			  </tr>
			  <tr>
			    <td>Birth Date</td>
			    <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
			    <td><fmt:formatDate pattern = "dd-MM-yyyy" value = "${customer.birthDate}" /></td>
			  </tr>
			</table>
			<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
              <h5>Accounts</h5>
              <a href="" data-toggle="modal" data-target="#addConfirm" onClick="onAdd('${customer.id}')" class="btn btn-sm btn-outline-primary">Add Account</a>
            </div>
            
            <c:choose>
            	<c:when test="${not empty message && message.status eq 'SUCCESS'}">
            		<div class="alert alert-success" role="alert">
            			${message.description}
                	</div>
				</c:when>
				<c:when test="${not empty message && message.status eq 'ERROR'}">
					<div class="alert alert-error" role="alert">
						${message.description}
					</div>
				</c:when>
			</c:choose>
			
              <div class="table-responsive">
              <c:choose>
              	<c:when test="${not empty customer.account}">
                <table class="table table-striped table-sm">
                  <thead>
                    <tr>
                      <th>Account Number</th>
                      <th>Balance</th>
                      <th>Status</th>
                      <th>&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<c:forEach var="account" varStatus="status" items="${customer.account}">
                   		<tr>
                   			<td>${account.accountNumber}</td>
                   			<td>${account.balance}</td>
                   			<td>${account.status}</td>
                   			<td>
                   				<a href="<c:url value='/account/mutasi/${account.accountNumber}'/>" class="btn btn-sm btn-outline-secondary">MUTASI</a>
								<c:choose>
									<c:when test="${account.status eq 'CLOSE'}">
                						<a>&nbsp;</a>
	            					</c:when>
	           						<c:otherwise>
	           							<a href="" data-toggle="modal" data-target="#closeConfirm" onClick="onClose('${account.accountNumber}')" class="btn btn-sm btn-outline-secondary">CLOSE</a>
	            					</c:otherwise>
								</c:choose>
							</td>
                       </tr>
                  	</c:forEach>
                  </tbody>
                </table>
                </c:when>
                <c:otherwise>
	               	<p>No Data</p>
	            </c:otherwise>
              </c:choose>
              </div>
          </form:form>
        </main>
      </div>
    </div>
    
    <div class="modal fade" id="addConfirm" tabindex="-1" role="dialog" aria-labelledby="addConfirmLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <c:url var="add_url"  value="/account/add" />
          <form action="${add_url}" method="post">
              <div class="modal-header">
                <h5 class="modal-title" id="addConfirmLabel">Add Account Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                Are you sure to create account for this customer?
                <input type="text" name="add_code" hidden="true"/>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary" id="btnAddConfirm">Yes</button>
              </div>
          </form>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="closeConfirm" tabindex="-1" role="dialog" aria-labelledby="closeConfirmLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <c:url var="close_url"  value="/account/close" />
          <form action="${close_url}" method="post">
              <div class="modal-header">
                <h5 class="modal-title" id="closeConfirmLabel">Close Account Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                Are you sure to close this account ?
                <input type="text" name="close_code" hidden="true"/>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary" id="btnCloseConfirm">Yes</button>
              </div>
          </form>
        </div>
      </div>
    </div>
    
    <script>
        $(document).ready(function(){
           feather.replace();
        });
        function onAdd(id) {
            $("input[type=text][name=add_code]").val(id);
        }
        function onClose(accountNumber) {
            $("input[type=text][name=close_code]").val(accountNumber);
        }
    </script>
</body>
</html>