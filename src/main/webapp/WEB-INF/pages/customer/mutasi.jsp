<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE HTML>
<html>
<head>
    <spring:url value="/static/js/jquery-3.4.1.min.js" var="jqueryJs" />
    <spring:url value="/static/css/bootstrap.min.css" var="bootstrapCss" />
    <spring:url value="/static/css/bootstrap-datepicker.css" var="bootstrapDatepickerCss" />
    <spring:url value="/static/js/bootstrap.min.js" var="bootstrapJs" />
    <spring:url value="/static/js/feather.min.js" var="featherJs" />
    <spring:url value="/static/js/bootstrap-datepicker.js" var="bootstrapDatepickerJs" />
    <link rel="stylesheet" type="text/css" href="${bootstrapCss}"/>
    <link rel="stylesheet" type="text/css" href="${bootstrapDatepickerCss}"/>
    <script src="${jqueryJs}"></script>
    <script src="${bootstrapJs}"></script>
    <script src="${featherJs}"></script>
    <script src="${bootstrapDatepickerJs}"></script>
</head>
<body>
	<jsp:include page="../layout/layout.jsp"></jsp:include>
	
	<div class="container-fluid">
		<div class="row">
			<nav class="col-md-2 d-none d-md-block bg-light sidebar">
				<div class="sidebar-sticky">
					<ul class="nav flex-column">
						<li class="nav-item">
							<a class="nav-link active" href="http://localhost:8085/MiniCell">
								<span data-feather="home"></span> Master Lookup
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="http://localhost:8085/MiniCell/customer">
								<strong><span data-feather="users"></span> Customers </strong>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="http://localhost:8085/MiniCell/topupaccount">
								<span data-feather="arrow-up-circle"></span> Top-up Account
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="http://localhost:8085/MiniCell/transfer">
								<span data-feather="refresh-cw"></span> Transfer within Account
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">
								<span data-feather="dollar-sign"></span> Bill Transaction
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">
								<span data-feather="dollar-sign"></span> Bill Payment
							</a>
						</li>
					</ul>
				</div>
			</nav>
    
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h4>Customer</h4>
          </div>
          
          <c:url var="search_url"  value="/account/mutasi/search" />
          <form:form id="form" action="${search_url}" method="get" modelAttribute="customer">
          	<table>
			  <tr>
			    <td>Customer Name</td>
			    <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
			    <td>${customer.name}</td>
			    <td>&nbsp;</td>
			    <td>Account Number</td>
			    <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
			    <td>${account.accountNumber}</td>
			    <form:input type="text" path="accountNumber" hidden="true" value="${account.accountNumber}"/>
			  </tr>
			  <tr>
			    <td>Identity Type</td>
			    <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
			    <td>${lookupDetail.description}</td>
			    <td>&nbsp;</td>
			    <td>Balance</td>
			    <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
			    <td>${account.balance}</td>
			  </tr>
			  <tr>
			    <td>Identity Number</td>
			    <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
			    <td>${customer.identityNumber}</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>
			  <tr>
			    <td>Birth Date</td>
			    <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
			    <td><fmt:formatDate pattern = "dd-MM-yyyy" value = "${customer.birthDate}" /></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>
			  <tr>
			    <td>Transaction Date</td>
			    <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
			    <td class="dates"><form:input id="user"  type="text" class="form-control" placeholder="dd-mm-yyy" path="transactionDate1"/></td>
			    <td>&nbsp;&nbsp;-&nbsp;&nbsp;</td>
			    <td class="dates"><form:input id="user"  type="text" class="form-control" placeholder="dd-mm-yyy" path="transactionDate2"/></td>
			    <td>&nbsp;</td>
			    <td><button type="submit" class="btn btn-primary">Search</button></td>
			  </tr>
			</table>
			<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
              <h5>Account Statement</h5>
            </div>
            
            <c:choose>
            	<c:when test="${not empty message && message.status eq 'SUCCESS'}">
            		<div class="alert alert-success" role="alert">
            			${message.description}
                	</div>
				</c:when>
				<c:when test="${not empty message && message.status eq 'ERROR'}">
					<div class="alert alert-error" role="alert">
						${message.description}
					</div>
				</c:when>
			</c:choose>
			
			<div class="table-responsive">
				<table class="table table-striped table-sm">
					<thead>
					  <tr>
					    <th>Transaction Date</th>
					    <th>Transaction Type</th>
					    <th>Berita</th>
					    <th>Amount</th>
					  </tr>
					</thead>
					<tbody>
   						<c:choose>
						<c:when test="${not empty mutasiList}">
							<c:forEach var="mutasi" items="${mutasiList}">
							<%-- <form:input type="text" path="mutasi" hidden="true" value="${mutasiList}"/> --%>
								<tr>
									<td><fmt:formatDate pattern = "dd-MM-yyyy" value = "${mutasi.transactionDate}" /></td>
									<td>${mutasi.transactionType}</td>
									<td>${mutasi.berita}</td>
									<td>${mutasi.amount}</td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<td colspan="4">No data</td>
							</tr>
						</c:otherwise>
						</c:choose>
         				</tbody>
       			</table>
     			</div>
          </form:form>
        </main>
      </div>
    </div>
    
    <script>
        $(document).ready(function(){
           feather.replace();
        });
        $(function(){
        	$('.dates #user').datepicker({
        		'format' : 'dd-mm-yyyy',
        		'autoclose' : true
        	})
        })
    </script>
</body>
</html>