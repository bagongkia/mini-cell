<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <spring:url value="/static/js/jquery-3.4.1.min.js" var="jqueryJs" />
    <spring:url value="/static/css/bootstrap.min.css" var="bootstrapCss" />
    <spring:url value="/static/js/bootstrap.min.js" var="bootstrapJs" />
    <spring:url value="/static/js/feather.min.js" var="featherJs" />
    <link rel="stylesheet" type="text/css" href="${bootstrapCss}"/>
    <script src="${jqueryJs}"></script>
    <script src="${bootstrapJs}"></script>
    <script src="${featherJs}"></script>
</head>
<body>
	<jsp:include page="../layout/layout.jsp"></jsp:include>
	
	<div class="container-fluid">
	<div class="row">
		<nav class="col-md-2 d-none d-md-block bg-light sidebar">
			<div class="sidebar-sticky">
				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link active" href="http://localhost:8085/MiniCell">
							<strong><span data-feather="home"></span> Master Lookup</strong>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="http://localhost:8085/MiniCell/customer">
							<span data-feather="users"></span> Customers
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="http://localhost:8085/MiniCell/topupaccount">
							<span data-feather="arrow-up-circle"></span> Top-up Account
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="http://localhost:8085/MiniCell/transfer">
							<span data-feather="refresh-cw"></span> Transfer within Account
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">
							<span data-feather="dollar-sign"></span> Bill Transaction
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">
							<span data-feather="dollar-sign"></span> Bill Payment
						</a>
					</li>
				</ul>
			</div>
		</nav>
    
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h4>Master Lookup</h4>
          </div>
          <form:form id="form" action="" method="post" modelAttribute="lookup">
              <div class="form-group">
                <label for="code">Code</label>
                <c:choose>
                    <c:when test="${not empty code}">
                        <form:input type="text" class="form-control" path="code" readonly="true"/>
                    </c:when>
                    <c:otherwise>
                        <form:input type="text" class="form-control" path="code"/>
                    </c:otherwise>
                </c:choose>
              </div>
              <div class="form-group">
                <label for="description">Description</label>
                <form:input type="text" class="form-control" path="description"/>
                <form:input path="createdTime" hidden="true"/>
              </div>
              <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h5>Detail</h5>
                <div class="btn-toolbar mb-2 mb-md-0">
                  <div class="btn-group mr-2">
                      <button type="button" class="btn btn-sm btn-outline-secondary" onClick="addDetail()">Add Detail</button>
                  </div>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table table-striped table-sm">
                  <thead>
                    <tr>
                      <th>Code</th>
                      <th>Description</th>
                      <th>&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                      <c:choose>
                          <c:when test="${not empty lookup.lookupDetail}">
                              <c:forEach var="detail" varStatus="status" items="${lookup.lookupDetail}">
                                <tr>
                                  <td><form:input class="form-control" path="lookupDetail[${status.index}].code"/></td>
                                  <td><form:input class="form-control" path="lookupDetail[${status.index}].description"/></td>
                                  <td><button onClick="deleteItem('${status.index}')"><span data-feather="trash">&nbsp;</span></button></td>
                                  <form:hidden class="form-control" path="lookupDetail[${status.index}].id"/>
                                </tr>
                            </c:forEach>
                          </c:when>
                      </c:choose>
                  </tbody>
                </table>
              </div>
              <button type="submit" class="btn btn-primary" onClick="onSubmit()">Submit</button>
              <form:input path="selected" hidden="true"/>
              <form:input path="action" hidden="true"/>
          </form:form>
        </main>
      </div>
    </div>
    
    <script>
        function addDetail() {
            $("input[type=text][name=action]").val("add_detail");
            $("#form").submit();
        }
        function deleteItem(index) {
            $("input[type=text][name=action]").val("delete_detail");
            $("input[type=text][name=selected]").val(index);
            $("#form").submit();
        }
        function onSubmit() {
            $("input[type=text][name=action]").val("submit");
        }
    
        $(document).ready(function(){
           feather.replace();
        });
    </script>
</body>
</html>