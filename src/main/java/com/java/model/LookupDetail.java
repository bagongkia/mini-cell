package com.java.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "mst_lookup_detail")
public class LookupDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lookupdetail_sequence")
    @SequenceGenerator(name="lookupdetail_sequence", sequenceName = "mst_lookup_detail_seq", allocationSize = 1)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "lookup_code")
    private Lookup lookup;
    
    @Column(name = "code", length = 25)
    private String code;
    
    @Column(name = "description", length = 255)
    private String description;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Lookup getLookup() {
        return lookup;
    }
    public void setLookup(Lookup lookup) {
        this.lookup = lookup;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
}
