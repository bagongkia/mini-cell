package com.java.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "mst_transfer")
public class Transfer {
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transfer_sequence")
    @SequenceGenerator(name="transfer_sequence", sequenceName = "mst_transfer_seq", allocationSize = 1)
    private Integer id;
	
	@Column(name = "transaction_date", length = 10)
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDate;
	
    @Column(name = "from_account")
    private Integer fromAccount;

    @Column(name = "to_account")
    private Integer toAccount;
	
	@Column(name = "amount")
    private Float amount;
	
	@Column(name = "fee")
    private Float fee;
	
	@Column(name = "berita", length = 255)
    private String berita;
	
	@Transient
    private Float amount1;
	
	@Transient
    private Float amount2;
	
	@Transient
    private String fromCustomer;
	
	@Transient
    private String toCustomer;
	
	@Transient
    private Integer selected;
    
    @Transient
    private String action;
	
	public Integer getSelected() {
		return selected;
	}

	public void setSelected(Integer selected) {
		this.selected = selected;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getFromCustomer() {
		return fromCustomer;
	}

	public void setFromCustomer(String fromCustomer) {
		this.fromCustomer = fromCustomer;
	}

	public String getToCustomer() {
		return toCustomer;
	}

	public void setToCustomer(String toCustomer) {
		this.toCustomer = toCustomer;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Integer getFromAccount() {
		return fromAccount;
	}

	public void setFromAccount(Integer fromAccount) {
		this.fromAccount = fromAccount;
	}

	public Integer getToAccount() {
		return toAccount;
	}

	public void setToAccount(Integer toAccount) {
		this.toAccount = toAccount;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	public Float getFee() {
		return fee;
	}

	public void setFee(Float fee) {
		this.fee = fee;
	}

	public String getBerita() {
		return berita;
	}

	public void setBerita(String berita) {
		this.berita = berita;
	}

	public Float getAmount1() {
		return amount1;
	}

	public void setAmount1(Float amount1) {
		this.amount1 = amount1;
	}

	public Float getAmount2() {
		return amount2;
	}

	public void setAmount2(Float amount2) {
		this.amount2 = amount2;
	}
	
	
}
