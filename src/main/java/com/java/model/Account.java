package com.java.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "mst_account")
public class Account {
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_sequence")
    @SequenceGenerator(name="account_sequence", sequenceName = "mst_account_seq", allocationSize = 1)
    @Column(name="account_number")
    private Integer accountNumber;
    
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;
    
    @Column(name = "balance")
    private Float balance;
    
    @Column(name = "status", length = 25)
    private String status;
    
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "account", orphanRemoval = true)
    private List<Topup> topup;

    
    public Integer getAccountNumber() {
		return accountNumber;
	}
    
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public Float getBalance() {
		return balance;
	}
	
	public void setBalance(Float balance) {
		this.balance = balance;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Customer getCustomer() {
        return customer;
    }
	
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
    public List<Topup> getTopup() {
        return topup;
    }
    
    public void setTopup(List<Topup> topup) {
        this.topup.addAll(topup);
    }	
    
}
