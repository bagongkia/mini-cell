package com.java.model;

import java.util.Date;

public class Mutasi {
	private Date transactionDate;
	private String transactionType;
	private String berita;
	private Float amount;
	
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getBerita() {
		return berita;
	}
	public void setBerita(String berita) {
		this.berita = berita;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	
	
}
