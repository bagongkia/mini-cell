package com.java.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "mst_topup")
public class Topup {
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "topup_sequence")
    @SequenceGenerator(name="topup_sequence", sequenceName = "mst_topup_seq", allocationSize = 1)
    private Integer id;
	
	@Column(name = "transaction_date", length = 10)
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDate;
	
	@ManyToOne
    @JoinColumn(name = "account_number")
    private Account account;
	
	@Column(name = "amount")
    private Float amount;
	
	@Column(name = "berita", length = 255)
    private String berita;
	
	@Transient
    private Float amount1;
	
	@Transient
    private Float amount2;
	
	@Transient
    private Integer selected;
    
    @Transient
    private String action;
    
    @Transient
    private String customerName;
    
    @Transient
    private Integer accountNumber;

	public Integer getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	public String getBerita() {
		return berita;
	}

	public void setBerita(String berita) {
		this.berita = berita;
	}

	public Float getAmount1() {
		return amount1;
	}

	public void setAmount1(Float amount1) {
		this.amount1 = amount1;
	}

	public Float getAmount2() {
		return amount2;
	}

	public void setAmount2(Float amount2) {
		this.amount2 = amount2;
	}

	public Integer getSelected() {
		return selected;
	}

	public void setSelected(Integer selected) {
		this.selected = selected;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	

}
