package com.java.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "mst_customer")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_sequence")
    @SequenceGenerator(name="customer_sequence", sequenceName = "mst_customer_seq", allocationSize = 1)
    private Integer id;
    
    @Column(name = "name", length = 255)
    private String name;
    
    @Column(name = "identity_type", length = 25)
    private String identityType;
    
    @Column(name = "identity_number", length = 100)
    private String identityNumber;
    
    @Column(name = "birth_date", length = 10)
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthDate;
    
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "customer", orphanRemoval = true)
    private List<Account> account;
    
    @Transient
    private String identityTypeDescription;
    
    @Transient
    private String selected;
    
    @Transient
    private String action;
    
    @Transient
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date transactionDate1;
    
    @Transient
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date transactionDate2;
    
    @Transient
    private String transactionType;
    
    @Transient
    private Integer accountNumber;
    
    @Transient
    private List<Mutasi> mutasi; 
    
    
	public List<Mutasi> getMutasi() {
		return mutasi;
	}

	public void setMutasi(List<Mutasi> mutasi) {
		this.mutasi.addAll(mutasi);
	}

	public Integer getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
    
    public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Date getTransactionDate1() {
		return transactionDate1;
	}

	public void setTransactionDate1(Date transactionDate1) {
		this.transactionDate1 = transactionDate1;
	}

	public Date getTransactionDate2() {
		return transactionDate2;
	}

	public void setTransactionDate2(Date transactionDate2) {
		this.transactionDate2 = transactionDate2;
	}

	public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getIdentityType() {
        return identityType;
    }
    
    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }
    
    public String getIdentityTypeDescription() {
        return identityTypeDescription;
    }
    
    public void setIdentityTypeDescription(String identityTypeDescription) {
        this.identityTypeDescription = identityTypeDescription;
    }
    
    public String getIdentityNumber() {
        return identityNumber;
    }
    
    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }
    
    public Date getBirthDate() {
        return birthDate;
    }
    
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
    
    public String getSelected() {
        return selected;
    }
    
    public void setSelected(String selected) {
        this.selected = selected;
    }
    
    public String getAction() {
        return action;
    }
    
    public void setAction(String action) {
        this.action = action;
    }
    
    public List<Account> getAccount() {
        return account;
    }
    
    public void setAccount(List<Account> account) {
        this.account.addAll(account);
    }
}