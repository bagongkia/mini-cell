package com.java.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.java.dto.CustomerDtoValidator;
import com.java.dto.Message;
import com.java.model.Customer;
import com.java.model.Lookup;
import com.java.model.LookupDetail;
import com.java.service.CustomerService;
import com.java.service.LookupService;

@Controller
@RequestMapping("/customer")
public class CustomerController {
	private static final Logger log = LogManager.getLogger(CustomerController.class);
	private static final String IDENTITY_TYPE = "IDENTITY_TYPE";
	
	@Autowired
    private CustomerService customerService;
	
	@Autowired
    private LookupService lookupService;
	
	@PreAuthorize("hasAuthority('USER')")
	@GetMapping("")
    public String list(@ModelAttribute("customer") Customer customer, Model model) {
        log.debug("customer list were called.");
        List<Customer> customerList = customerService.findAll();
        model.addAttribute("customerList", customerList);
        
        return "customer/list";
    }
	
	@GetMapping("/create")
    public String create(Model model) {
        Customer customer = new Customer();
        model.addAttribute("customer", customer);
        
        Lookup lookup = lookupService.find(IDENTITY_TYPE);
        if (lookup != null) {
            model.addAttribute("identityTypeList", lookup.getLookupDetail());
        }
        return "customer/edit";
    }
	
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable(name = "id") Integer id, Model model) {
        Customer customer = customerService.find(id);
        model.addAttribute("customer", customer);
        Lookup lookup = lookupService.find(IDENTITY_TYPE);
        if (lookup != null) {
            model.addAttribute("identityTypeList", lookup.getLookupDetail());
        }
        return "customer/edit";
    }
	
	@GetMapping("/{id}")
    public String detail(@PathVariable(name = "id") Integer id, Model model) {
		Customer customer = customerService.find(id);
		LookupDetail lookupDetail = lookupService.find(IDENTITY_TYPE, customer.getIdentityType());
		if (lookupDetail != null) {
			model.addAttribute("lookupDetail", lookupDetail);
        }
		log.debug("Identity Type : " + lookupDetail.getDescription());
        model.addAttribute("customer", customer);
        return "customer/detail";
    }
	
	@PostMapping(path = {"/create", "/edit/{identityNumber}"})
    public String create(@ModelAttribute("customer") @Validated Customer customer, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
		if ("choose_identity_type".equals(customer.getAction())) {
            log.debug("Choosen Identity Type: {}", customer.getSelected());
            customer.setIdentityType(customer.getSelected());
            customer.setIdentityTypeDescription(lookupService.find(IDENTITY_TYPE, customer.getSelected()).getDescription());
        } else if ("submit".equals(customer.getAction())) {
        	log.error("Error : " + bindingResult.getAllErrors());
        	if(bindingResult.hasErrors()){
        		Lookup lookup = lookupService.find(IDENTITY_TYPE);
                if (lookup != null) {
                    model.addAttribute("identityTypeList", lookup.getLookupDetail());
                }
        		return "customer/edit";
        	}else return submit(customer, redirectAttributes);
        }
        Lookup lookup = lookupService.find(IDENTITY_TYPE);
        if (lookup != null) {
            model.addAttribute("identityTypeList", lookup.getLookupDetail());
        }
        return "customer/edit";
    }
	
	@GetMapping("/search")
    public String search(@ModelAttribute("customer") Customer customer, Model model) {
		List<Customer> customerList = customerService.search(customer);
    	model.addAttribute("customerList", customerList);
        return "customer/list";
    }
	
	private String submit(@ModelAttribute("customer") Customer customer, RedirectAttributes redirectAttributes) {
		
		customerService.save(customer);
        
        Message message = new Message();
        message.setStatus("SUCCESS");
        message.setDescription("Changes were submitted successfully.");
        redirectAttributes.addFlashAttribute("message", message);
        
        return "redirect:/customer";
    }
	
//	@InitBinder
//    protected void initBinder(WebDataBinder binder) {
//        binder.setValidator( new CustomerDtoValidator());
//    }
}
