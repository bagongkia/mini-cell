package com.java.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.java.dto.LookupDto;
import com.java.dto.Message;
import com.java.model.Lookup;
import com.java.model.LookupDetail;
import com.java.service.LookupServiceImpl;

@Controller
@RequestMapping("/lookup")
public class LookupController {
	private static final Logger log = LogManager.getLogger(LookupController.class);
	
	@Autowired
    private LookupServiceImpl lookupService;
	
	@GetMapping("/create")
    public String create(Model model) {
        Lookup lookup = new Lookup();
        model.addAttribute("lookup", lookup);
        return "lookup/edit";
    }
	
	@GetMapping("/edit/{code}")
    public String edit(@PathVariable(name = "code") String code, Model model) {
        Lookup lookup = lookupService.find(code);
        model.addAttribute("lookup", lookup);
        return "lookup/edit";
    }
	
	@PostMapping(path = {"/create", "/edit/{code}"})
    public String edit(@ModelAttribute("lookup") Lookup lookup, RedirectAttributes redirectAttributes) {
        if ("add_detail".equals(lookup.getAction())) {
            if (lookup.getLookupDetail() == null) {
                lookup.setLookupDetail(new ArrayList<>());
            }
            LookupDetail lookupDetail = new LookupDetail();
            lookupDetail.setLookup(lookup);
            lookup.getLookupDetail().add(lookupDetail);
        } else if ("delete_detail".equals(lookup.getAction())) {
            log.debug("Delete item detail with index: {}", lookup.getSelected());
            lookup.getLookupDetail().remove(Integer.parseInt(lookup.getSelected()));
        } else if ("submit".equals(lookup.getAction())) {
            log.debug("Submit lookup");
            return submit(lookup, redirectAttributes);
        }
        return "lookup/edit";
    }
	
	@GetMapping("/{code}")
    public String detail(@PathVariable(name = "code") String code, Model model) {
        Lookup lookup = lookupService.find(code);
        model.addAttribute("lookup", lookup);
        return "lookup/detail";
    }
	
	@PostMapping("/delete")
    public String delete(@RequestParam(name = "deleted_code") String code, RedirectAttributes redirectAttributes) {
        lookupService.delete(code);
        
        Message message = new Message();
        message.setStatus("SUCCESS");
        message.setDescription(code + " were deleted successfully.");
        redirectAttributes.addFlashAttribute("message", message);
        
        return "redirect:/";
    }
	
	@GetMapping("/search")
    public String search(@ModelAttribute("lookupDto") LookupDto lookupDto, Model model) {
    	List<Lookup> lookupList = lookupService.search(lookupDto);
    	model.addAttribute("lookupList", lookupList);
        return "index";
    }
	
	private String submit(@ModelAttribute("lookup") Lookup lookup, RedirectAttributes redirectAttributes) {
        lookupService.save(lookup);
        
        Message message = new Message();
        message.setStatus("SUCCESS");
        message.setDescription("Changes were submitted successfully.");
        redirectAttributes.addFlashAttribute("message", message);
        
        return "redirect:/";
    }
}
