package com.java.controller;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.java.dto.Message;
import com.java.model.Account;
import com.java.model.Transfer;
import com.java.service.AccountService;
import com.java.service.TransferService;

@Controller
@RequestMapping("/transfer")
public class TransferController {
	private static final Logger log = LogManager.getLogger(TransferController.class);
	
	@Autowired
	private TransferService transferService;
	
	@Autowired
	private AccountService accountService;
	
	@GetMapping("")
    public String list(@ModelAttribute("transfer") Transfer transfer, Model model) {
        log.debug("transfer list were called.");
        List<Transfer> transferList = transferService.findAll();
        model.addAttribute("transferList", transferList);
        return "transfer/list";
    }
	
	@GetMapping("/transfer")
    public String create(Model model) {
        Transfer transfer = new Transfer();
        model.addAttribute("transfer", transfer);
        
        List<Account> account = accountService.findAccount();
        if (account != null) {
            model.addAttribute("fromAccountList", account);
            model.addAttribute("toAccountList", account);
        }
        return "transfer/transfer";
    }
	
	@PostMapping("/transfer")
    public String create(@ModelAttribute("transfer") @Validated Transfer transfer, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
		if ("choose_from_account".equals(transfer.getAction())) {
            Account account = accountService.find(transfer.getSelected());
            transfer.setFromAccount(account.getAccountNumber());
            transfer.setFromCustomer(account.getCustomer().getName());
        } else if ("choose_to_account".equals(transfer.getAction())) {
            log.debug("Choosen Account: {}", transfer.getSelected());
            Account account = accountService.find(transfer.getSelected());
            log.debug("Choosen Account: {}", account.getCustomer().getName());
            transfer.setToAccount(account.getAccountNumber());
            transfer.setToCustomer(account.getCustomer().getName());
        }
		else if ("submit".equals(transfer.getAction())) {
        	return submit(transfer, redirectAttributes);
        }
		List<Account> account = accountService.findAccount();
        if (account != null) {
            model.addAttribute("fromAccountList", account);
            model.addAttribute("toAccountList", account);
        }
        if(transfer.getFromAccount() != null && transfer.getToAccount() != null) {
        	if(transfer.getFromCustomer().equals(transfer.getToCustomer())) {
        		transfer.setFee(0.0f);
        	}else transfer.setFee(1500.0f);
        }
        return "transfer/transfer";
    }
	
	@GetMapping("/search")
    public String search(@ModelAttribute("transfer") Transfer transfer, Model model) {
		if(transfer.getFromAccount() == null && transfer.getAmount1() == null && transfer.getAmount2() == null) {
			return "redirect:/transfer";
		}
		if(transfer.getAmount1() == null) {
			transfer.setAmount1(0.0f);
		}
		if(transfer.getAmount2() == null) {
			transfer.setAmount2(999999999999999.0f);
		}
		log.debug("amount 1 = " + transfer.getAmount1() + "  amount 2 = " + transfer.getAmount2());
		List<Transfer> transferList = transferService.search(transfer);
    	model.addAttribute("transferList", transferList);
        return "transfer/list";
    }
	
	private String submit(@ModelAttribute("transfer") Transfer transfer, RedirectAttributes redirectAttributes) {
		Date date = new Date();
		Float fromBalance;
		Float toBalance;
		
		Account fromAccount = accountService.find(transfer.getFromAccount());
		log.debug("From Account Balance : " + fromAccount.getBalance() + "    Fee : " + transfer.getFee()  +  "   ALL : " + (transfer.getAmount() + transfer.getFee()));
		if(fromAccount.getBalance() == 0.0f || fromAccount.getBalance() < (transfer.getAmount() + transfer.getFee())) {
			Message message = new Message();
	        message.setStatus("ERROR");
	        message.setDescription("Insufficient Balance.");
	        redirectAttributes.addFlashAttribute("message", message);
	        
	        return "redirect:/transfer";
		}
		
		Account toAccount = accountService.find(transfer.getToAccount());
		
		Transfer t = new Transfer();
		t.setTransactionDate(date);
		t.setFromAccount(transfer.getFromAccount());
		t.setToAccount(transfer.getToAccount());
		t.setAmount(transfer.getAmount());
		t.setFee(transfer.getFee());
		t.setBerita(transfer.getBerita());
		
		fromBalance = fromAccount.getBalance() - (transfer.getAmount() + transfer.getFee());
		fromAccount.setBalance(fromBalance);
		
		toBalance = toAccount.getBalance() + transfer.getAmount();
		toAccount.setBalance(toBalance);
		
		transferService.save(t);
        
        Message message = new Message();
        message.setStatus("SUCCESS");
        message.setDescription("Transferred Successfully.");
        redirectAttributes.addFlashAttribute("message", message);
        
        return "redirect:/transfer";
    }
}
