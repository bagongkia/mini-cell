package com.java.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.java.dto.Message;
import com.java.model.Account;
import com.java.model.Topup;
import com.java.service.AccountService;
import com.java.service.TopupService;

@Controller
@RequestMapping("/topupaccount")
public class TopupController {
	private static final Logger log = LogManager.getLogger(TopupController.class);
	
	@Autowired
    private TopupService topupService;
	
	@Autowired
	private AccountService accountService;
	
	@GetMapping("")
    public String list(@ModelAttribute("topup") Topup topup, Model model) {
        log.debug("topup list were called.");
        List<Topup> topupList = topupService.findAll();
        model.addAttribute("topupList", topupList);
        return "topup/list";
    }
	
	@GetMapping("/topup")
    public String create(Model model) {
        Topup topup = new Topup();
        model.addAttribute("topup", topup);
        
        List<Account> account = accountService.findAccount();
        if (account != null) {
            model.addAttribute("accountList", account);
        }
        return "topup/topup";
    }
	
	@PostMapping("/topup")
    public String create(@ModelAttribute("topup") @Validated Topup topup, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
		if ("choose_account".equals(topup.getAction())) {
            log.debug("Choosen Account: {}", topup.getSelected());
            Account account = accountService.find(topup.getSelected());
            log.debug("Choosen Account: {}", account.getCustomer().getName());
            topup.setAccountNumber(account.getAccountNumber());
            topup.setCustomerName(account.getCustomer().getName());
        } else if ("submit".equals(topup.getAction())) {
        	return submit(topup, redirectAttributes);
        }
		List<Account> account = accountService.findAll();
        if (account != null) {
            model.addAttribute("accountList", account);
        }
        return "topup/topup";
    }
	
	@GetMapping("/search")
    public String search(@ModelAttribute("topup") Topup topup, Model model) {
		if(topup.getAccountNumber() == null && topup.getAmount1() == null && topup.getAmount2() == null) {
			return "redirect:/topupaccount";
		}
		if(topup.getAmount1() == null) {
			topup.setAmount1(0.0f);
		}
		if(topup.getAmount2() == null) {
			topup.setAmount2(999999999999999.0f);
		}
		log.debug("amount 1 = " + topup.getAmount1() + "  amount 2 = " + topup.getAmount2());
		List<Topup> topupList = topupService.search(topup);
    	model.addAttribute("topupList", topupList);
        return "topup/list";
    }
	
	private String submit(@ModelAttribute("topup") Topup topup, RedirectAttributes redirectAttributes) {
		Date date = new Date();
		Float balance;
		log.debug("date : " + date );
		
		Account account = accountService.find(topup.getAccountNumber());
		
		if (account.getTopup() == null) {
          account.setTopup(new ArrayList<>());
		}
		
		Topup t = new Topup();
		t.setTransactionDate(date);
		t.setAccount(account);
		t.setAmount(topup.getAmount());
		t.setBerita(topup.getBerita());
		
		account.getTopup().add(t);
		balance = account.getBalance() + topup.getAmount();
		account.setBalance(balance);
		
		accountService.save(account);
        
        Message message = new Message();
        message.setStatus("SUCCESS");
        message.setDescription("Changes were submitted successfully.");
        redirectAttributes.addFlashAttribute("message", message);
        
        return "redirect:/topupaccount";
    }
}
