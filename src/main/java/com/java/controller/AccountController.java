package com.java.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.java.dto.Message;
import com.java.model.Account;
import com.java.model.Customer;
import com.java.model.LookupDetail;
import com.java.model.Mutasi;
import com.java.model.Topup;
import com.java.model.Transfer;
import com.java.service.AccountService;
import com.java.service.CustomerService;
import com.java.service.LookupServiceImpl;
import com.java.service.TopupService;
import com.java.service.TransferService;

@Controller
@RequestMapping("/account")
public class AccountController {
	private static final Logger log = LogManager.getLogger(AccountController.class);
	
	@Autowired
    private CustomerService customerService;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
    private LookupServiceImpl lookupService;
	
	@Autowired
    private TopupService topupService;
	
	@Autowired
	private TransferService transferService;
	
	@PostMapping("/add")
    public String add(@RequestParam(name = "add_code") Integer id, RedirectAttributes redirectAttributes) {
		Customer customer = customerService.find(id);
		
		if (customer.getAccount() == null) {
          customer.setAccount(new ArrayList<>());
		}
		
		Account account = new Account();
		account.setCustomer(customer);
		account.setBalance(0.0f);
		account.setStatus("ACTIVE");
        customer.getAccount().add(account);
        customerService.save(customer);
        
        Message message = new Message();
        message.setStatus("SUCCESS");
        message.setDescription("Account were add successfully.");
        redirectAttributes.addFlashAttribute("message", message);
        
        return "redirect:/customer/" + id;
    }
	
	@PostMapping("/close")
    public String close(@RequestParam(name = "close_code") Integer accountNumber, RedirectAttributes redirectAttributes) {
		Account account = accountService.find(accountNumber);
		Customer customer = account.getCustomer();
		log.debug(account.getStatus());
		account.setStatus("CLOSE");
        customerService.save(customer);
        
        Message message = new Message();
        message.setStatus("SUCCESS");
        message.setDescription("Account were add successfully.");
        redirectAttributes.addFlashAttribute("message", message);
        
        return "redirect:/customer/" + customer.getId();
    }
	
	@GetMapping("/mutasi/{accountNumber}")
    public String edit(@PathVariable(name = "accountNumber") Integer accountNumber, Model model) {
        Account account = accountService.find(accountNumber);
		Customer customer = customerService.find(account.getCustomer().getId());
        model.addAttribute("customer", customer);
        model.addAttribute("account", account);
        LookupDetail lookupDetail = lookupService.find("IDENTITY_TYPE", customer.getIdentityType());
		if (lookupDetail != null) {
			model.addAttribute("lookupDetail", lookupDetail);
        }
		
		List<Mutasi> mutasiList = new ArrayList<>();
		
		List<Topup> topup = topupService.findTopup(accountNumber);
		for(Topup t: topup) {
			Mutasi mutasi = new Mutasi();
			mutasi.setTransactionDate(t.getTransactionDate());
			mutasi.setTransactionType("Top-up");
			mutasi.setBerita(t.getBerita());
			mutasi.setAmount(t.getAmount());
			mutasiList.add(mutasi);
		}
		
		List<Transfer> transfer = transferService.findTransfer(accountNumber);
		for(Transfer t: transfer) {
			log.debug("accountNumber : " + accountNumber + "    t.getFromAccountNumber() :" + t.getFromAccount());
			if(accountNumber.equals(t.getFromAccount())) {
				Mutasi mutasi = new Mutasi();
				mutasi.setTransactionDate(t.getTransactionDate());
				mutasi.setTransactionType("Transfer Out");
				mutasi.setBerita(t.getBerita());
				mutasi.setAmount(t.getAmount());
				mutasiList.add(mutasi);
			}else {
				Mutasi mutasi = new Mutasi();
				mutasi.setTransactionDate(t.getTransactionDate());
				mutasi.setTransactionType("Transfer In");
				mutasi.setBerita(t.getBerita());
				mutasi.setAmount(t.getAmount());
				mutasiList.add(mutasi);
			}
		}
		
		model.addAttribute("mutasiList", mutasiList);
        return "customer/mutasi";
    }
	
	@GetMapping("/mutasi/search")
    public String search(@ModelAttribute("customer") Customer customer, Model model) {
//        log.debug("mutasi testing" + mutasi.size());
		Account account = accountService.find(customer.getAccountNumber());
		Customer cus = customerService.find(account.getCustomer().getId());
        model.addAttribute("customer", cus);
        model.addAttribute("account", account);
        LookupDetail lookupDetail = lookupService.find("IDENTITY_TYPE", cus.getIdentityType());
		if (lookupDetail != null) {
			model.addAttribute("lookupDetail", lookupDetail);
        }
		
		List<Mutasi> mutasiList = new ArrayList<>();
		
		if(customer.getTransactionDate1() == null && customer.getTransactionDate2() == null) {
			List<Topup> topup = topupService.findTopup(customer.getAccountNumber());
			for(Topup t: topup) {
				Mutasi mutasi = new Mutasi();
				mutasi.setTransactionDate(t.getTransactionDate());
				mutasi.setTransactionType("Top-up");
				mutasi.setBerita(t.getBerita());
				mutasi.setAmount(t.getAmount());
				mutasiList.add(mutasi);
			}
			
			List<Transfer> transfer = transferService.findTransfer(customer.getAccountNumber());
			for(Transfer t: transfer) {
				log.debug("accountNumber : " + customer.getAccountNumber() + "    t.getFromAccountNumber() :" + t.getFromAccount());
				if(customer.getAccountNumber().equals(t.getFromAccount())) {
					Mutasi mutasi = new Mutasi();
					mutasi.setTransactionDate(t.getTransactionDate());
					mutasi.setTransactionType("Transfer Out");
					mutasi.setBerita(t.getBerita());
					mutasi.setAmount(t.getAmount());
					mutasiList.add(mutasi);
				}else {
					Mutasi mutasi = new Mutasi();
					mutasi.setTransactionDate(t.getTransactionDate());
					mutasi.setTransactionType("Transfer In");
					mutasi.setBerita(t.getBerita());
					mutasi.setAmount(t.getAmount());
					mutasiList.add(mutasi);
				}
			}
			
			model.addAttribute("mutasiList", mutasiList);
	        return "customer/mutasi";
		}
		
		if(customer.getTransactionDate2() == null) {
			List<Topup> topup = topupService.findTopup(customer.getAccountNumber());
			for(Topup t: topup) {
				if(t.getTransactionDate().after(customer.getTransactionDate1())) {
					Mutasi mutasi = new Mutasi();
					mutasi.setTransactionDate(t.getTransactionDate());
					mutasi.setTransactionType("Top-up");
					mutasi.setBerita(t.getBerita());
					mutasi.setAmount(t.getAmount());
					mutasiList.add(mutasi);
				}
				
			}
			
			List<Transfer> transfer = transferService.findTransfer(customer.getAccountNumber());
			for(Transfer t: transfer) {
				log.debug("accountNumber : " + customer.getAccountNumber() + "    t.getFromAccountNumber() :" + t.getFromAccount());
				if(customer.getAccountNumber().equals(t.getFromAccount())) {
					if(t.getTransactionDate().after(customer.getTransactionDate1())) {
						Mutasi mutasi = new Mutasi();
						mutasi.setTransactionDate(t.getTransactionDate());
						mutasi.setTransactionType("Transfer Out");
						mutasi.setBerita(t.getBerita());
						mutasi.setAmount(t.getAmount());
						mutasiList.add(mutasi);
					}
				}else {
					if(t.getTransactionDate().after(customer.getTransactionDate1())) {
						Mutasi mutasi = new Mutasi();
						mutasi.setTransactionDate(t.getTransactionDate());
						mutasi.setTransactionType("Transfer In");
						mutasi.setBerita(t.getBerita());
						mutasi.setAmount(t.getAmount());
						mutasiList.add(mutasi);
					}
				}
			}
			
			model.addAttribute("mutasiList", mutasiList);
	        return "customer/mutasi";
		}
		
		if(customer.getTransactionDate1() == null) {
			List<Topup> topup = topupService.findTopup(customer.getAccountNumber());
			for(Topup t: topup) {
				if(t.getTransactionDate().before(customer.getTransactionDate2())) {
					Mutasi mutasi = new Mutasi();
					mutasi.setTransactionDate(t.getTransactionDate());
					mutasi.setTransactionType("Top-up");
					mutasi.setBerita(t.getBerita());
					mutasi.setAmount(t.getAmount());
					mutasiList.add(mutasi);
				}
				
			}
			
			List<Transfer> transfer = transferService.findTransfer(customer.getAccountNumber());
			for(Transfer t: transfer) {
				log.debug("accountNumber : " + customer.getAccountNumber() + "    t.getFromAccountNumber() :" + t.getFromAccount());
				if(customer.getAccountNumber().equals(t.getFromAccount())) {
					if(t.getTransactionDate().before(customer.getTransactionDate2())) {
						Mutasi mutasi = new Mutasi();
						mutasi.setTransactionDate(t.getTransactionDate());
						mutasi.setTransactionType("Transfer Out");
						mutasi.setBerita(t.getBerita());
						mutasi.setAmount(t.getAmount());
						mutasiList.add(mutasi);
					}
				}else {
					if(t.getTransactionDate().before(customer.getTransactionDate2())) {
						Mutasi mutasi = new Mutasi();
						mutasi.setTransactionDate(t.getTransactionDate());
						mutasi.setTransactionType("Transfer In");
						mutasi.setBerita(t.getBerita());
						mutasi.setAmount(t.getAmount());
						mutasiList.add(mutasi);
					}
				}
			}
			
			model.addAttribute("mutasiList", mutasiList);
	        return "customer/mutasi";
		}
		else {
			
			List<Topup> topup = topupService.findTopup(customer.getAccountNumber());
			for(Topup t: topup) {
				if(t.getTransactionDate().after(customer.getTransactionDate1()) && t.getTransactionDate().before(customer.getTransactionDate2())) {
					Mutasi mutasi = new Mutasi();
					mutasi.setTransactionDate(t.getTransactionDate());
					mutasi.setTransactionType("Top-up");
					mutasi.setBerita(t.getBerita());
					mutasi.setAmount(t.getAmount());
					mutasiList.add(mutasi);
				}
				
			}
			
			List<Transfer> transfer = transferService.findTransfer(customer.getAccountNumber());
			for(Transfer t: transfer) {
				log.debug("accountNumber : " + customer.getAccountNumber() + "    t.getFromAccountNumber() :" + t.getFromAccount());
				if(customer.getAccountNumber().equals(t.getFromAccount())) {
					if(t.getTransactionDate().after(customer.getTransactionDate1()) && t.getTransactionDate().before(customer.getTransactionDate2())) {
						Mutasi mutasi = new Mutasi();
						mutasi.setTransactionDate(t.getTransactionDate());
						mutasi.setTransactionType("Transfer Out");
						mutasi.setBerita(t.getBerita());
						mutasi.setAmount(t.getAmount());
						mutasiList.add(mutasi);
					}
				}else {
					if(t.getTransactionDate().after(customer.getTransactionDate1()) && t.getTransactionDate().before(customer.getTransactionDate2())) {
						Mutasi mutasi = new Mutasi();
						mutasi.setTransactionDate(t.getTransactionDate());
						mutasi.setTransactionType("Transfer In");
						mutasi.setBerita(t.getBerita());
						mutasi.setAmount(t.getAmount());
						mutasiList.add(mutasi);
					}
				}
			}
			
			model.addAttribute("mutasiList", mutasiList);
	        return "customer/mutasi";
		}
	}
}
