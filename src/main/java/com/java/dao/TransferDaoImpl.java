package com.java.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.java.model.Transfer;

@Repository
public class TransferDaoImpl implements TransferDao {
	@Autowired
    private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
    @Transactional(readOnly = true)
    public List<Transfer> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from Transfer").list();
    }
	
	@Override
    @Transactional
    public void save(Transfer transfer) {
        sessionFactory.getCurrentSession().merge(transfer);
    }
	
	@SuppressWarnings("unchecked")
    @Override
    @Transactional(readOnly = true)
    public List<Transfer> search(Transfer transfer) {
		if(transfer.getFromAccount()==null) {
			return sessionFactory.getCurrentSession()
	    			.createQuery("from Transfer WHERE amount >= " + transfer.getAmount1().intValue() + " AND amount <= " + transfer.getAmount2().intValue()).list();
		} else return sessionFactory.getCurrentSession()
    			.createQuery("from Transfer WHERE CAST (from_account AS text) LIKE '%" + transfer.getFromAccount() + "%' OR CAST (to_account AS text) LIKE '%" + transfer.getFromAccount() + "%' AND amount >= " + transfer.getAmount1().intValue() + " AND amount <= " + transfer.getAmount2().intValue()).list();
    }
	
	@SuppressWarnings("unchecked")
	@Override
    @Transactional(readOnly = true)
    public List<Transfer> findTransfer(Integer accountNumber) {
        return sessionFactory.getCurrentSession().createQuery("from Transfer where from_account = :accountNumber OR to_account = :accountNumber")
        		.setParameter("accountNumber", accountNumber)
        		.list();
    }
}
