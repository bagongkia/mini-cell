package com.java.dao;

import com.java.model.User;

public interface UserDao {
    public User find(String username);
}