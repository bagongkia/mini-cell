package com.java.dao;

import java.util.List;

import com.java.dto.LookupDto;
import com.java.model.Lookup;
import com.java.model.LookupDetail;

public interface LookupDao {
	List<Lookup> findAll();
	void save(Lookup lookup);
    Lookup find(String code);
    void delete(Lookup lookup);
    List<Lookup> search(LookupDto lookupDto);
    LookupDetail find(String lookupCode, String code);
}
