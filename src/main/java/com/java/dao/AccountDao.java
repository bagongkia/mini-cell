package com.java.dao;

import java.util.List;

import com.java.model.Account;

public interface AccountDao {
	public Account find(Integer accountNumber);
	public List<Account> findAll();
	public List<Account> findAccount();
	public void save(Account account);
}
