package com.java.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.java.model.Topup;

@Repository
public class TopupDaoImpl implements TopupDao {
	@Autowired
    private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
    @Transactional(readOnly = true)
    public List<Topup> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from Topup").list();
    }
	
	@SuppressWarnings("unchecked")
    @Override
    @Transactional(readOnly = true)
    public List<Topup> search(Topup topup) {
		if(topup.getAccountNumber()==null) {
			return sessionFactory.getCurrentSession()
	    			.createQuery("from Topup WHERE amount >= " + topup.getAmount1().intValue() + " AND amount <= " + topup.getAmount2().intValue()).list();
		} else return sessionFactory.getCurrentSession()
    			.createQuery("from Topup WHERE CAST (account_number AS text) LIKE '%" + topup.getAccountNumber() + "%' AND amount >= " + topup.getAmount1().intValue() + " AND amount <= " + topup.getAmount2().intValue()).list();
    }
	
	@SuppressWarnings("unchecked")
	@Override
    @Transactional(readOnly = true)
    public List<Topup> findTopup(Integer accountNumber) {
        return sessionFactory.getCurrentSession().createQuery("from Topup where account_number = :accountNumber")
        		.setParameter("accountNumber", accountNumber)
        		.list();
    }
}
