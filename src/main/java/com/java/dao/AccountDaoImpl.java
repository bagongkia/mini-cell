package com.java.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.java.model.Account;

@Repository
public class AccountDaoImpl implements AccountDao {
	@Autowired
    private SessionFactory sessionFactory;
	
	@Override
    @Transactional(readOnly = true)
    public Account find(Integer accountNumber) {
        return sessionFactory.getCurrentSession().get(Account.class, accountNumber);
    }
	
	@SuppressWarnings("unchecked")
	@Override
    @Transactional(readOnly = true)
    public List<Account> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from Account").list();
    }
	
	@SuppressWarnings("unchecked")
	@Override
    @Transactional(readOnly = true)
    public List<Account> findAccount() {
        return sessionFactory.getCurrentSession().createQuery("from Account where status <> 'CLOSE'").list();
    }
	
	@Override
    @Transactional
    public void save(Account account) {
        sessionFactory.getCurrentSession().saveOrUpdate(account);
    }
}
