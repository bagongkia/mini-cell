package com.java.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.java.model.Customer;

@Repository
public class CustomerDaoImpl implements CustomerDao {
	@Autowired
    private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
    @Override
    @Transactional(readOnly = true)
    public List<Customer> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from Customer").list();
    }
	
	@Override
    @Transactional
    public void save(Customer customer) {
        sessionFactory.getCurrentSession().saveOrUpdate(customer);
    }
	
	@SuppressWarnings("unchecked")
    @Override
    @Transactional(readOnly = true)
    public List<Customer> search(Customer customer) {
    	return sessionFactory.getCurrentSession().createQuery("from Customer WHERE name LIKE '%" + customer.getName() + "%' AND identityNumber LIKE '%" + customer.getIdentityNumber() + "%' ").list();
    }
	
	@Override
    @Transactional(readOnly = true)
    public Customer find(Integer id) {
        return sessionFactory.getCurrentSession().get(Customer.class, id);
    }
}
