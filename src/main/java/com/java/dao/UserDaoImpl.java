package com.java.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.java.model.User;

@Repository
public class UserDaoImpl implements UserDao {
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    @Transactional(readOnly = true)
    public User find(String username) {
        return sessionFactory.getCurrentSession().get(User.class, username);
    }
}
