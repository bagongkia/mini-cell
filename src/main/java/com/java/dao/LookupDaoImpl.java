package com.java.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.java.dto.LookupDto;
import com.java.model.Lookup;
import com.java.model.LookupDetail;

@Repository
public class LookupDaoImpl implements LookupDao {
	@Autowired
    private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
    @Override
    @Transactional(readOnly = true)
    public List<Lookup> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from Lookup").list();
    }
	
	@Override
    @Transactional
    public void save(Lookup lookup) {
        sessionFactory.getCurrentSession().saveOrUpdate(lookup);
    }
	
    @Override
    @Transactional(readOnly = true)
    public Lookup find(String code) {
        return sessionFactory.getCurrentSession().get(Lookup.class, code);
    }
    
    @Override
    @Transactional
    public void delete(Lookup lookup) {
        sessionFactory.getCurrentSession().delete(lookup);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(readOnly = true)
    public List<Lookup> search(LookupDto lookupDto) {
    	return sessionFactory.getCurrentSession().createQuery("from Lookup WHERE code LIKE '%" + lookupDto.getCode() + "%' AND description LIKE '%" + lookupDto.getDescription() + "%' ").list();
    }
    
    @Override
    public LookupDetail find(String lookupCode, String code) {
        return (LookupDetail) sessionFactory.getCurrentSession()
                .createQuery("from LookupDetail where lookup.code = :lookupCode and code = :code")
                .setParameter("lookupCode", lookupCode)
                .setParameter("code", code)
                .uniqueResult();
    }
}
