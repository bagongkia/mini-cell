package com.java.dao;

import java.util.List;

import com.java.model.Topup;

public interface TopupDao {
	public List<Topup> findAll();
	public List<Topup> search(Topup topup);
	public List<Topup> findTopup(Integer accountNumber);
}
