package com.java.dao;

import java.util.List;

import com.java.model.Customer;

public interface CustomerDao {
	public List<Customer> findAll();
	public void save(Customer customer);
	public List<Customer> search(Customer customer);
	public Customer find(Integer id);
}
