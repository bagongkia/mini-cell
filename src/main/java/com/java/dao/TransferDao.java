package com.java.dao;

import java.util.List;

import com.java.model.Transfer;

public interface TransferDao {
	public List<Transfer> findAll();
	public void save(Transfer transfer);
	public List<Transfer> search(Transfer transfer);
	public List<Transfer> findTransfer(Integer accountNumber);
}
