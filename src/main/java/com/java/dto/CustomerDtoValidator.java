package com.java.dto;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.java.model.Customer;

public class CustomerDtoValidator implements Validator {
	public boolean supports(Class<?> candidate) {
        return Customer.class.isAssignableFrom(candidate);
    }
	
	public void validate(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required", "Field is required.");
//        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "identityType", "required", "Field is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "identityNumber", "required", "Field is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "birthDate", "required", "Field is required.");
        
//        Customer customer = (Customer) obj;
//        logger.log(Level.INFO, student.getAge().toString());
//        if (student.getAge() >= 13 && student.getAge() < 18) {
////        	logger.log(Level.WARN, "Umur kurang dari 18 tahun");
//            errors.rejectValue("age", "age.invalid", "Age restriction.");
//        }
//        if(student.getAge() < 13) {
////        	logger.log(Level.ERROR, "Umur kurang dari 13 tahun");
//            errors.rejectValue("age", "age.invalid", "Age restriction.");
//        }
    }
}
