package com.java;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.java.dto.LookupDto;
import com.java.model.Lookup;
import com.java.service.LookupServiceImpl;

@Controller
public class IndexController {
	private static final Logger log = LogManager.getLogger(IndexController.class);
	
	@Autowired
    private LookupServiceImpl lookupService;
	
	@GetMapping("/")
	public String list(@ModelAttribute("lookupDto") LookupDto lookupDto, Model model) {
        log.debug("lookup list were called.");
        List<Lookup> lookupList = lookupService.findAll();
        model.addAttribute("lookupList", lookupList);
        
        return "index";
    }
}

