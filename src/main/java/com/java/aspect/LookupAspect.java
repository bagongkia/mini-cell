package com.java.aspect;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import com.java.model.Customer;
import com.java.model.Lookup;
@Aspect
public class LookupAspect {
    private static final Logger log = LogManager.getLogger(LookupAspect.class);
    
    @Before("execution(* com.java.service.LookupServiceImpl.findAll(..))")
    public void beforeSelect() {
        log.debug("Hi, from Aspect Before");
    }
    
    @After("execution(* com.java.service.LookupServiceImpl.findAll(..))")
    public void afterSelect() {
        log.debug("Hi, from Aspect After");
    }
    
    @Pointcut(value="execution(* com.java.service.LookupServiceImpl.save(..))")
    public void doArround() {
    	
    }
    
    @Around("doArround()")
    public Object saveArround(ProceedingJoinPoint pjp) throws Throwable {
        Object[] args = pjp.getArgs();

        if (args[0] instanceof Lookup) {
            Lookup entity = (Lookup) args[0];
            if(entity.getCreatedTime() == null) {
            	entity.setCreatedTime(new Date());
            	log.debug("Created Time : " + entity.getCreatedTime());
                log.debug("Updated Time : " + entity.getUpdatedTime());
            	return pjp.proceed(args); 
            }
            entity.setUpdatedTime(new Date());
            log.debug("Created Time : " + entity.getCreatedTime());
            log.debug("Updated Time : " + entity.getUpdatedTime());
        }
        return pjp.proceed(args); 
    }
}