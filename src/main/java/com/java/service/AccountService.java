package com.java.service;

import java.util.List;

import com.java.model.Account;

public interface AccountService {
	public Account find(Integer accountNumber);
	public List<Account> findAll();
	public List<Account> findAccount();
	public void save(Account account);
}
