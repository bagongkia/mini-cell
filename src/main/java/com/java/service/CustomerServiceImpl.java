package com.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.dao.CustomerDao;
import com.java.model.Customer;

@Service
public class CustomerServiceImpl implements CustomerService {
	@Autowired
    private CustomerDao customerDao;
	
	public List<Customer> findAll() {
        return customerDao.findAll();
    }
	
	@Override
    public void save(Customer customer) {
        customerDao.save(customer);
    }
	
	public List<Customer> search(Customer customer) {
        return customerDao.search(customer);
    }

	public Customer find(Integer id) {
        return customerDao.find(id);
    }
}
