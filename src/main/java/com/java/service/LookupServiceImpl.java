package com.java.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.dao.LookupDao;
import com.java.dto.LookupDto;
import com.java.model.Lookup;
import com.java.model.LookupDetail;

@Service
public class LookupServiceImpl implements LookupService {
	private static final Logger log = LogManager.getLogger(LookupServiceImpl.class);
	
	@Autowired
    private LookupDao lookupDao;
	
	public List<Lookup> findAll() {
        return lookupDao.findAll();
    }
	
	public void save(Lookup lookup) {
        //Important: lookupDetail must not be null
        if (lookup.getLookupDetail() == null) {
            log.debug("Set new array list for lookup detail.");
            lookup.setLookupDetail(new ArrayList<>());
        }
        lookup.getLookupDetail().forEach(lookupDetail -> lookupDetail.setLookup(lookup));
        lookupDao.save(lookup);
    }
	
	public Lookup find(String code) {
        return lookupDao.find(code);
    }
    
    public void delete(String code) {
        Lookup lookup = lookupDao.find(code);
        lookupDao.delete(lookup);
    }
    
    public List<Lookup> search(LookupDto dto) {
        return lookupDao.search(dto);
    }
    
    @Override
    public LookupDetail find(String lookupCode, String code) {
        return lookupDao.find(lookupCode, code);
    }
}
