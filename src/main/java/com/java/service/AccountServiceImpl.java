package com.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.dao.AccountDao;
import com.java.model.Account;

@Service
public class AccountServiceImpl implements AccountService {
	
	@Autowired
    private AccountDao accountDao;
	
	public Account find(Integer accountNumber) {
        return accountDao.find(accountNumber);
    }
	
	public List<Account> findAll() {
        return accountDao.findAll();
    }
	
	public List<Account> findAccount() {
        return accountDao.findAccount();
    }
	
	@Override
    public void save(Account account) {
        accountDao.save(account);
    }
}
