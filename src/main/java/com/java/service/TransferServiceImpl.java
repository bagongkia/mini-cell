package com.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.dao.TransferDao;
import com.java.model.Transfer;

@Service
public class TransferServiceImpl implements TransferService{
	@Autowired
    private TransferDao transferDao;
	
	public List<Transfer> findAll() {
        return transferDao.findAll();
    }
	
	@Override
    public void save(Transfer transfer) {
        transferDao.save(transfer);
    }
	
	public List<Transfer> search(Transfer transfer) {
        return transferDao.search(transfer);
    }
	
	public List<Transfer> findTransfer(Integer accountNumber) {
        return transferDao.findTransfer(accountNumber);
    }
}
