package com.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.dao.TopupDao;
import com.java.model.Topup;

@Service
public class TopupServiceImpl implements TopupService {
	@Autowired
    private TopupDao topupDao;
	
	public List<Topup> findAll() {
        return topupDao.findAll();
    }
	
	public List<Topup> search(Topup topup) {
        return topupDao.search(topup);
    }
	
	public List<Topup> findTopup(Integer accountNumber) {
        return topupDao.findTopup(accountNumber);
    }
}
