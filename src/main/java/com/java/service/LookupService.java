package com.java.service;

import java.util.List;

import com.java.dto.LookupDto;
import com.java.model.Lookup;
import com.java.model.LookupDetail;

public interface LookupService {
	public List<Lookup> findAll();
	public void save(Lookup lookup);
	public Lookup find(String code);
    public void delete(String code);
    public List<Lookup> search(LookupDto dto);
    public LookupDetail find(String lookupCode, String code);
}
