package com.java.service;

import com.java.model.User;

public interface UserDetailsService {
	public User find(String username);
}
