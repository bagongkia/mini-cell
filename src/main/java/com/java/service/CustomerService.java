package com.java.service;

import java.util.List;

import com.java.model.Customer;

public interface CustomerService {
	public List<Customer> findAll();
	public void save(Customer customer);
	public List<Customer> search(Customer customer);
	public Customer find(Integer id);
}
