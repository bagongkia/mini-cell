package com.java.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import com.java.dao.UserDao;
import com.java.model.User;

public class UserDetailsServiceImpl implements UserDetailsService {
    
    private static final Logger log = LogManager.getLogger(UserDetailsServiceImpl.class);
    @Autowired
    private UserDao userDao;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.find(username);
        
        if (user != null) {
            UserBuilder userBuilder = org.springframework.security.core.userdetails.User.withUsername(user.getUsername())
                    .password(user.getPassword())
                    .authorities("USER");
            
            return userBuilder.build();
        }
        throw new UsernameNotFoundException(username + " not found");
    }
}
